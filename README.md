# Project 2 Data Capture

This script captures data from Ambient Wind Sensor and stores in a csv file for use with my second project in Data Visualization.

## Purpsoe

This script assumes the local environment variables have been created that contain the API key and Application Key for the Ambient Weather API.  It will not work without these keys. This script makes use of the AIOAmbient python package to connect to the realt time API.


