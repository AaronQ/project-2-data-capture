# Explores the data to determine statistics.
import copy

import numpy as np
import pandas as pd
import statistics
from visualization import create_graphs

def read_file(file_name):
  """
  reads in the file and puts the data into lists
  :param file_name: name of the input file
  :return: flower_meas: list of lists containing list of  measurement values
  """
  with open(file_name, 'r') as f:
    variables = f.readline().rstrip().split(',')
    flower_meas = []
    for line in f:
      line_txt = line.rstrip().split(',')
      flower_meas.append(line_txt)
  return flower_meas, variables


def min_values(vector):
  """
  determines the minimum value for a list
  :param vector: list of values
  :return: min_value: minimum value of vector
  """
  min_value = vector[0]
  for i in range(0, len(vector)):
    min_value = vector[i] if vector[i] < min_value else min_value
  return min_value


def max_values(vector):
  """
  determines the minimum value for a list
  :param vector: list of values
  :return: min_value: minimum value of vector
  """
  max_value = vector[0]
  for i in range(0, len(vector)):
    max_value = vector[i] if vector[i] > max_value else max_value
  return max_value


def mean_calc(vector):
  """
  determines the mean for list of values
  :param vector: list of the values
  :result mean_value: mean of vector values
  """
  mean_value = float(vector[0])
  for i in range(1, len(vector)):
    mean_value = mean_value + float(vector[i])
  mean_value = round(mean_value/len(vector), 4)
  return mean_value


def median_calc(vector):
  """
  determines the median for list of values
  :param vector: list of the values
  :result median_value: median of vector values
  """
  median_value = float(vector[0])
  for i in range(1, len(vector)):
    mean_value = mean_value + float(vector[i])
  mean_value = round(mean_value/len(vector), 4)
  return median_value

def trim_mean_calc(vector, p):
  """
  calculates the mean for vector values after the first and last p number of elements are removed
  from the sorted list
  :param vector: list of lists containing values
  :param p: number of elements to trim
  :return: trim_value: list of lists containing the trim mean values
  """
  values = copy.deepcopy(vector)
  values.sort()
  del values[-p:]
  del values[:p]
  values = np.array(values)
  trim_value = float(values[0])
  for i in range(1, len(values) - 1):
    trim_value = trim_value + float(values[i])
  trim_value = round(trim_value/len(values), 4)
  return trim_value


def stan_dev(vector, mean_value):
  """
  Calculates the standard deviation of the vector data
  :param data_matrix: list of lists containing values
  :param mean_value: mean value of the vector
  :return: stan_dev: list containing the standard deviation values
  """
  stan_dev = 0
  for i in range(0, len(vector)):
    values = float(vector[i]) - mean_value
    values = values ** 2
    stan_dev = stan_dev + values
  stan_dev = (stan_dev / (len(vector) - 1))
  stan_dev = stan_dev ** (1/2)
  return round(stan_dev, 4)


def skew_calc(vector, mean_value):
  """
  Calculates the skewness of the vector data
  :param vector: list containing values
  :param mean_value: mean value of vector
  :return: skew_val: skewness values
  """
  skew_val = 0
  stan_dev = 0
  for i in range(0, len(vector)):
    values = float(vector[i]) - mean_value
    stan_val = values ** 2
    values = values ** 3
    skew_val = skew_val + values
    stan_dev = stan_dev + stan_val
  skew_val = (skew_val / len(vector))
  stan_dev = (stan_dev / (len(vector)))
  stan_dev = stan_dev ** (3/2)
  if stan_dev == 0:
      return 0
  skew_val = skew_val/stan_dev
  return round(skew_val, 4)

def average_direction(speed, direction):
    """Calculate the average wind direction using wind speed and direction"""
    direction_ew = [speed[i]*np.sin(direction[i]*np.pi/180) for i in range(0, len(direction), 1)]
    direction_ew_average = sum(direction_ew)/len(direction_ew)
    direction_ns = [speed[i]*np.cos(direction[i]*np.pi/180) for i in range(0, len(direction), 1)]
    direction_ns_average = sum(direction_ns)/len(direction_ns)
    direction_average = np.arctan2(direction_ew_average,direction_ns_average) * 180/np.pi
    if direction_average < 0:
        direction_average = direction_average + 360
    return round(direction_average, 4)


def kurtosis_calc(vector, mean_value):
  """
  Calculates the skewness of the vector data
  :param vector: list containing values
  :param mean_value: mean value of vector
  :return: kurt_val: kurtosis values
  """
  kurt_val = 0
  stan_dev = 0
  for i in range(0, len(vector)):
    values = float(vector[i]) - mean_value
    stan_val = values ** 2
    values = values ** 4
    kurt_val = kurt_val + values
    stan_dev = stan_dev + stan_val
  kurt_val = (kurt_val / len(vector))
  stan_dev = (stan_dev / (len(vector)))
  stan_dev = stan_dev ** (4/2)
  if stan_dev == 0:
      return 0
  kurt_val = kurt_val/stan_dev
  return round(kurt_val, 4)

if __name__ == '__main__':
  file_name = 'LakeNW Record.csv'
  total_meas, variables = read_file(file_name)
  total_meas = list(map(list, zip(*total_meas)))

  # Convert values of measurements to numbers
  total_meas[1] = [float(x) for x in total_meas[1]]
  total_meas[2] = [float(x) for x in total_meas[2]]
  total_meas[3] = [float(x) for x in total_meas[3]]
  total_meas[4] = [float(x) for x in total_meas[4]]
  total_meas[5] = [float(x) for x in total_meas[5]]
  total_meas[7] = [float(x) for x in total_meas[7]]
  total_meas[8] = [float(x) for x in total_meas[8]]
  total_meas[9] = [float(x) for x in total_meas[9]]
  total_meas[10] = [float(x) for x in total_meas[10]]

  # remove measurements for when no wind is present
  for i in range(len(total_meas[3]) - 1, -1, -1):
      if total_meas[3][i] == 0:
        del total_meas[0][i]
        del total_meas[1][i]
        del total_meas[2][i]
        del total_meas[3][i]
        del total_meas[4][i]
        del total_meas[5][i]
        del total_meas[6][i]
        del total_meas[7][i]
        del total_meas[8][i]
        del total_meas[9][i]
        del total_meas[10][i]


  # calculate minimums
  total_mins = ['-', min_values(total_meas[1]), min_values(total_meas[2]), min_values(total_meas[3]),
                min_values(total_meas[4]), min_values(total_meas[5]), '-', min_values(total_meas[7]),
                min_values(total_meas[8]),min_values(total_meas[9]), min_values(total_meas[10])]
  # calculate maximums
  total_maxs = ['-', max_values(total_meas[1]), max_values(total_meas[2]), max_values(total_meas[3]),
                max_values(total_meas[4]), max_values(total_meas[5]), '-', max_values(total_meas[7]),
                max_values(total_meas[8]), max_values(total_meas[9]), max_values(total_meas[10])]
 # calculate means
  total_means = ['-', mean_calc(total_meas[1]), mean_calc(total_meas[2]), mean_calc(total_meas[3]),
                 mean_calc(total_meas[4]), average_direction(total_meas[3], total_meas[5]), '-', mean_calc(total_meas[7]),
                 mean_calc(total_meas[8]), mean_calc(total_meas[9]), mean_calc(total_meas[10])]
  # calculate trimmed means
  total_trim_means = ['-', trim_mean_calc(total_meas[1], 2), trim_mean_calc(total_meas[2], 2),
                      trim_mean_calc(total_meas[3], 2), trim_mean_calc(total_meas[4], 2),
                      '-', '-', trim_mean_calc(total_meas[7], 2),
                      trim_mean_calc(total_meas[8], 2), trim_mean_calc(total_meas[9], 2),
                      trim_mean_calc(total_meas[10], 2)]
  # caclulate the mode
  total_modes = ['-', statistics.mode(total_meas[1]), statistics.mode(total_meas[2]), statistics.mode(total_meas[3]),
                 statistics.mode(total_meas[4]), statistics.mode(total_meas[5]), statistics.mode(total_meas[6]),
                 statistics.mode(total_meas[7]), statistics.mode(total_meas[8]), statistics.mode(total_meas[9]),
                 statistics.mode(total_meas[10])]
  # calculate the median
  total_median = ['-', statistics.median(total_meas[1]), statistics.median(total_meas[2]),
                  statistics.median(total_meas[3]), statistics.median(total_meas[4]),
                  statistics.median(total_meas[5]), '-', statistics.median(total_meas[7]),
                  statistics.median(total_meas[8]), statistics.median(total_meas[9]), statistics.median(total_meas[10])]
  # calculate standard deviations
  total_stan_deviat = ['-', stan_dev(total_meas[1], total_means[1]),
                       stan_dev(total_meas[2], total_means[2]), stan_dev(total_meas[3], total_means[3]),
                       stan_dev(total_meas[4], total_means[4]), stan_dev(total_meas[5], total_means[5]), '-',
                       stan_dev(total_meas[7], total_means[7]), stan_dev(total_meas[8], total_means[8]),
                       stan_dev(total_meas[9], total_means[9]), stan_dev(total_meas[10], total_means[10])]
   # calculate skewness
  total_skew = ['-', skew_calc(total_meas[1], total_means[1]),
                skew_calc(total_meas[2], total_means[2]), skew_calc(total_meas[3], total_means[3]),
                skew_calc(total_meas[4], total_means[4]), skew_calc(total_meas[5], total_means[5]), '-',
                skew_calc(total_meas[7], total_means[7]), skew_calc(total_meas[8], total_means[8]),
                skew_calc(total_meas[9], total_means[9]), skew_calc(total_meas[10], total_means[10])]
  # calculate kurtosis
  total_kurt = ['-', kurtosis_calc(total_meas[1], total_means[1]),
                kurtosis_calc(total_meas[2], total_means[2]), kurtosis_calc(total_meas[3], total_means[3]),
                kurtosis_calc(total_meas[4], total_means[5]), kurtosis_calc(total_meas[5], total_means[5]), '-',
                kurtosis_calc(total_meas[7], total_means[7]), kurtosis_calc(total_meas[8], total_means[8]),
                kurtosis_calc(total_meas[9], total_means[9]), kurtosis_calc(total_meas[10], total_means[10])]

  # specify vectors containing column headers and flower types
  # create and print out dataframe for total data set
  data_type = ['Continuous', 'Continuous', 'Continuous', 'Continuous', 'Continuous', 'Continuous', 'Nominal',
               'Continuous', 'Continuous', 'Continuous', 'Continuous']
  description = ['Date and Time of Reading', 'Outside Temperature', 'Temperature that dew would form',
                'Current Wind Speed', 'Maximum Wind Speed in last 10 minutes', 'Direction of wind with North being 0',
                'Wind General Direction', 'Amount of Rain in last hour', 'Humidity', 'Solar Radiation',
                'Absolute Air Pressure']
  data = {'Variable' : variables, 'Data Type' : data_type, 'Description' : description, 'Minimum' : total_mins,
          'Maximum' : total_maxs, 'Mean' : total_means, 'Trimmed Means' : total_trim_means, 'Mode' : total_modes,
          'Median' : total_median, 'Standard Deviation' : total_stan_deviat, 'Skewness' : total_skew,
          'Kurtosis' : total_kurt}
  total_df = pd.DataFrame(data).set_index('Variable')

  print(total_df)
  # create and print out dataframe for class data sets
  total_df.to_csv('output.csv', index=True, mode='w')

  # create plots to answer analysis questions.
  create_graphs(total_meas)