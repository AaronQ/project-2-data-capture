# This is the initial attempt to connect to the Ambient Weather API and pull data from Lake NW sensor

import datetime
import os
import asyncio
from aioambient import Websocket
from aioambient.errors import WebsocketError

macaddress_name = []


def print_data(data):
    """Write data to file when received"""
    if data.get('windspeedmph') is None:
        with open(find_name(data.get('macAddress')) + 'No Connection Record.csv', 'a+') as f:
            f.write('No connection to sensor at ' + str(datetime.datetime.fromtimestamp(data.get('dateutc') / 1000)) +
                    '\n')
            f.close()
    else:
        with open(find_name(data.get('macAddress')) + 'Record.csv', 'a') as f:
            # Check if data is not present

            f.write(str(datetime.datetime.fromtimestamp(data.get('dateutc') / 1000)) + ',')
            f.write(str(data.get('tempf')) + ',')
            f.write(str(data.get('dewPoint')) + ',')
            f.write(str(data.get('windspeedmph')) + ',')
            f.write(str(data.get('windgustmph')) + ',')
            f.write(str(data.get('winddir')) + ',')
            f.write(str(general_direction(data.get('winddir'))) + ',')
            f.write(str(data.get('hourlyrainin')) + ',')
            f.write(str(data.get('humidity')) + ',')
            f.write(str(data.get('solarradiation')) + ',')
            f.write(str(data.get('baromabsin')) + '\n')
            f.close()


def find_name(macaddress):
    device_name = [x[1] for x in macaddress_name if x[0] == macaddress]
    return device_name[0]


def print_goodbye():
    """Print a simple "goodbye" message."""
    print("Client has disconnected from the websocket")


def print_hello():
    """Print a simple "hello" message."""
    print("Client has connected to the websocket")

def general_direction(wind_direction):
    """Get the general direction of the wind based on the """
    if wind_direction >= 0 and wind_direction < 23:
        return 'North'
    elif wind_direction >= 23 and wind_direction < 68:
        return 'Northeast'
    elif wind_direction >= 68 and wind_direction < 113:
        return 'East'
    elif wind_direction >= 113 and wind_direction < 158:
        return 'Southeast'
    elif wind_direction >= 158 and wind_direction < 203:
        return 'South'
    elif wind_direction >= 203 and wind_direction < 248:
        return 'Southwest'
    elif wind_direction >= 248 and wind_direction < 293:
        return 'West'
    elif wind_direction >= 293 and wind_direction < 338:
        return 'Northwest'
    elif wind_direction >= 338 and wind_direction < 360:
        return 'North'


def print_subscribed(data):
    """Write out subscription data as it is received."""
    for device in data.get('devices'):
        macaddress_name.append([device.get('macAddress'), device.get('info').get('name')])
        if os.path.exists(os.getcwd() + '\\' + device.get('info').get('name') + ' Record.csv'):
            return
        with open(device.get('info').get('name') + 'Record.csv', 'a+') as f:
            f.write('Date and Time,Temperature (\u00b0F),Dew Point (\u00b0F),Wind Speed (mph),'
                    'Wind Gust (mph),Wind Direction (Degrees),Wind General Direction,'
                    'Hourly Rain (in/hr),Outdoor Humidity (%),Solar Radiation (W/m^2),'
                    'Absolute Pressure (inHG)\n')
            f.write(str(datetime.datetime.fromtimestamp(device.get('lastData').get('dateutc') / 1000)) + ',')
            f.write(str(device.get('lastData').get('tempf')) + ',')
            f.write(str(device.get('lastData').get('dewPoint')) + ',')
            f.write(str(device.get('lastData').get('windspeedmph')) + ',')
            f.write(str(device.get('lastData').get('windgustmph')) + ',')
            f.write(str(device.get('lastData').get('winddir')) + ',')
            f.write(str(general_direction(device.get('lastData').get('winddir'))) + ',')
            f.write(str(device.get('lastData').get('hourlyrainin')) + ',')
            f.write(str(device.get('lastData').get('humidity')) + ',')
            f.write(str(device.get('lastData').get('solarradiation')) + ',')
            f.write(str(device.get('lastData').get('baromabsin')) + '\n')
            f.close()


async def main() -> None:
    """Run the websocket example."""
    websocket = Websocket(app_key, api_key)
    websocket.on_connect(print_hello)
    websocket.on_disconnect(print_goodbye)
    websocket.on_subscribed(print_subscribed)
    websocket.on_data(print_data)

    try:
        await websocket.connect()
    except WebsocketError as err:
        print("There was a websocket error: %s", err)
        return

    while True:
        # print("Simulating some other task occurring...")
        await asyncio.sleep(5)


if __name__ == '__main__':
    # initialize variables from environment
    try:
        endpoint = os.environ["AMBIENT_ENDPOINT"]
        rt_endpoint = os.environ["AMBIENT_RT_ENDPOINT"]
        app_key = os.environ["AMBIENT_APPLICATION_KEY"]
        api_key = os.environ["AMBIENT_API_KEY"]
    except KeyError:
        pass

    # Connect to the Real Time API and pull continuous data
    asyncio.run(main())
