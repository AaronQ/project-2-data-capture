import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import matplotlib.cm as cm
from math import pi
from windrose import WindroseAxes
from collections import Counter
from matplotlib.lines import Line2D
import matplotlib.patches as mpatches
from dateutil import parser
import datetime


def plot_quiver(speed, direction, gen_dir, vector_color, gen_dir_types):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='polar')
    ax.set_theta_direction(-1)
    ax.set_theta_zero_location("N")
    # Plot origin (agent's start point)
    ax.plot(0, 0, color='black', marker='o', markersize=5)


    # Create lines to make custom legend
    legend_lines = []
    for i in range(0, len(vector_color), 1):
        legend_lines.append(Line2D([0], [0], color=vector_color[i], lw=4))
    # Plot agent's path
    for i in range(0, len(speed), 1):
        u = speed[i]/speed.max() * np.sin(np.deg2rad(direction[i]))
        v = speed[i]/speed.max() * np.cos(np.deg2rad(direction[i]))
        ax.quiver(0, 0, u, v,
                  color=vector_color[gen_dir_types.index(gen_dir[i])],
                  angles='uv', scale=2)
        value = 2
        n = np.sqrt(value**2+value**2)
    # Plot configuration
    ax.set_rmin(0)
    ax.set_rmax(speed.max())
    ax.set_thetalim(0, 2*np.pi)
    ax.set_xticks(np.linspace(0, 2*np.pi, 4, endpoint=False))
    ax.grid(True)
    ax.set_rlabel_position(45)
    plt.yticks([1,2,3,4,5,6,7,8],['1 mph', '2 mph', '3 mph', '4 mph', '5 mph', '6 mph', '7 mph', '8 mph'])
    plt.title('Wind Direction and Speed')
    plt.legend(legend_lines, gen_dir_types, loc='lower left')
    plt.show()


def scatter_plot(speed, direction, gen_dir, vector_color, gen_dir_types):
    color_map = []
    for i in range(0, len(gen_dir), 1):
        color_map.append(vector_color[gen_dir_types.index(gen_dir[i])])
    legend_handles = []
    plt.scatter(direction, speed, c=color_map)
    for i in range(0, len(vector_color), 1):
        legend_handles.append(mpatches.Rectangle((0,0),1,1, fc=vector_color[i]))
    plt.legend(legend_handles, gen_dir_types)
    plt.title('Wind Speed vs Direction')
    plt.ylabel('Wind Speed (MPH)').set_fontsize(12)
    plt.xlabel('Wind Direction (Degrees from North)').set_fontsize(12)
    plt.margins(.01, .03)
    plt.show()


def scatter_time(time, speed, direction, gen_dir, vector_color, gen_dir_types):
    color_map = []
    for i in range(0, len(gen_dir), 1):
        color_map.append(vector_color[gen_dir_types.index(gen_dir[i])])
    legend_handles = []
    plt.scatter(time, speed, c=color_map)
    for i in range(0, len(vector_color), 1):
        legend_handles.append(mpatches.Rectangle((0,0),1,1, fc=vector_color[i]))
    plt.legend(legend_handles, gen_dir_types)
    plt.title('Wind Speed vs Time')
    plt.ylabel('Wind Speed (MPH)').set_fontsize(12)
    plt.xlabel('Time (minutes)').set_fontsize(12)
    plt.xticks(np.arange(len(time)), '')
    plt.margins(.01, .03)
    plt.show()


def plotbar_speed(time, speed, direction, vector_color, gen_dir_types):
    """plot time vs wind speed in bar graph with direction indicators"""
    # calculate the average wind speed and wind direction in 30 minute increments
    test = time[len(time)-1]
    speed_average = [sum(speed[i:i+30])//30 for i in range(0, len(speed), 30)]
    direction_average = average_direction(speed, direction)
    gen_direction_average = [general_direction(direction_average[i]) for i in range(0, len(direction_average), 1)]

    # create time increments
    first_time = roundTime(parser.parse(time[0]), 1800)
    time_average = [(first_time + datetime.timedelta(minutes=30 * i)).strftime('%m/%d %H:%M') for i
                    in range(0, len(direction_average), 1)]
    # remove date from time labels
    time_labels = [(first_time + datetime.timedelta(minutes=30 * i)).strftime('%H:%M') for i
                    in range(0, len(direction_average), 1)]
    # remove times when wind average speed is zero
    zero_wind = [i for i in range(0, len(speed_average), 1) if speed_average[i] == 0]
    for i in range(len(zero_wind)-1, -1, -1):
        del speed_average[zero_wind[i]]
        del gen_direction_average[zero_wind[i]]
        del time_average[zero_wind[i]]
        del time_labels[zero_wind[i]]

    color_map = []
    for i in range(0, len(gen_direction_average), 1):
        color_map.append(vector_color[gen_dir_types.index(gen_direction_average[i])])
    legend_handles = []
    for i in range(0, len(vector_color), 1):
        legend_handles.append(mpatches.Rectangle((0, 0), 1, 1, fc=vector_color[i]))
    plt.figure(figsize=(10, 5))
    plt.bar(time_average, speed_average, color=color_map)
    plt.xticks(np.arange(len(time_average)), '')
    plt.ylabel('Wind Speed (mph)').set_fontsize(12)
    plt.xlabel('Sample Size (30 reading increments)').set_fontsize(12)
    plt.title('Wind Speed vs Time').set_fontsize(16)
    plt.margins(.01, .03)
    plt.legend(legend_handles, gen_dir_types)
    plt.show()


def plotbar_direction(direction, vector_color):
    """plot number of readings in a particular wind direction on a bar graph"""
    reading_directions = Counter(direction)
    plt.figure(figsize=(10, 5))
    bars = plt.bar(reading_directions.keys(), reading_directions.values(), color=vector_color)
    plt.title('Number of Readings per Wind Direction').set_fontsize(16)
    plt.ylabel('Number of Readings').set_fontsize(12)
    plt.xlabel('Wind Direction').set_fontsize(12)
    plt.show()


def scatter_direction(time, speed, direction, gen_dir, vector_color, gen_dir_types):
    color_map = []
    for i in range(0, len(gen_dir), 1):
        color_map.append(vector_color[gen_dir_types.index(gen_dir[i])])
    legend_handles = []
    plt.scatter(time, direction, c=color_map)
    for i in range(0, len(vector_color), 1):
        legend_handles.append(mpatches.Rectangle((0,0),1,1, fc=vector_color[i]))
    plt.legend(legend_handles, gen_dir_types)
    plt.title('Wind Direction vs Time')
    plt.ylabel('Wind Direction (MPH)').set_fontsize(12)
    plt.xlabel('Time (minutes)').set_fontsize(12)
    plt.xticks(np.arange(len(time)), '')
    plt.margins(.01, .03)
    plt.show()


def plot_windrose(speed, direction):
    """plot wind speed and direction on a windrose axes"""
    ax = WindroseAxes.from_ax()
    color_map = ['#ffd7cc', '#ffb8a5', '#f7775e', '#c20073', '#ee523c', '#e41a1c']
    ax.bar(direction, speed, nsector=8, bins=np.arange(speed.min(), speed.max(), speed.max()/6), colors=color_map,
           edgecolor='black')
    ax.set_legend(loc='lower left', units='mph')
    plt.title('Wind Direction and Speed')
    plt.show()


def plotpie_direction(direction, vector_color):
    reading_directions = Counter(direction)
    plt.pie(reading_directions.values(), labels=reading_directions.keys(), colors=vector_color, autopct='%1.1f%%')
    plt.title("Wind Direction by Percentage of Readings")
    plt.show()


def plotbar_winds(time, speed, gusts):
    """ creates a double bar graph showing wind speeds and wind gusts"""
    # Get average speeds over 30 minute intervals
    t_interval = 60
    speed_average = [sum(speed[i:i + t_interval]) // t_interval for i in range(0, len(speed), t_interval)]
    gusts_average = [sum(gusts[i:i + t_interval]) // t_interval for i in range(0, len(gusts), t_interval)]
    # Create time intervals of t_interval minutes
    first_time = roundTime(parser.parse(time[0]), 60*t_interval)
    time_average = [(first_time + datetime.timedelta(minutes=t_interval * i)).strftime('%m/%d %H:%M') for i
                    in range(0, len(speed_average), 1)]
    # remove date from time labels
    time_labels = [(first_time + datetime.timedelta(minutes=t_interval * i)).strftime('%H:%M') for i
                    in range(0, len(time_average), 1)]
    width = 0.35
    speed_bar = plt.bar(np.arange(len(time_average)), speed_average, width=width,  color='#f77351',
                        label='Average Speed (mph)')
    gust_bar = plt.bar(np.arange(len(time_average)) + width, gusts_average, width=width,  color='#5584b6',
                       label='Average Gust Speed (mph)')
    plt.title('Wind Speed vs Time').set_fontsize(16)
    plt.ylabel('Wind Speed (mph)').set_fontsize(12)
    plt.xlabel('Sample Size (' + str(t_interval) + ' minute reading increments)').set_fontsize(12)
    plt.xticks(np.arange(len(time_average)), '', rotation=90, fontsize=8)
    plt.margins(.01, .03)
    plt.legend()
    plt.show()


def filled_winds(time, speed, gusts):
    """Create Fill Plot of wind speeds and gust speeds"""
    # Get average speeds over 30 minute intervals
    speed_average = [sum(speed[i:i + 30]) // 30 for i in range(0, len(speed), 30)]
    gusts_average = [sum(gusts[i:i + 30]) // 30 for i in range(0, len(gusts), 30)]
    # Create time intervals of 30 minutes
    first_time = roundTime(parser.parse(time[0]), 1800)
    time_average = [(first_time + datetime.timedelta(minutes=30 * i)).strftime('%m/%d %H:%M') for i
                    in range(0, len(speed_average), 1)]
    # remove date from time labels
    first_time = roundTime(parser.parse(time[0]), 1800)
    time_labels = [(first_time + datetime.timedelta(minutes=i)).strftime('%H:%M') for i
                    in range(0, len(time), 1)]
    time_labels_average = [(first_time + datetime.timedelta(minutes=30 * i)).strftime('%H:%M') for i
                    in range(0, len(time_average), 1)]
    plt.fill_between(time, speed, 0, label='Wind Speed (mph)', color='#f77351')
    plt.fill_between(time, gusts, speed, label='Wind Gust Speed (mph)', color='#5584b6')
    plt.title('Wind Speed vs Time')
    plt.xticks(np.arange(len(time_labels)), time_labels, rotation=90, fontsize=8)
    plt.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False)
    plt.legend()
    plt.title('Wind Speed vs Time')
    plt.ylabel('Wind Speed (mph)').set_fontsize(12)
    plt.xlabel('Time (minutes)').set_fontsize(12)
    plt.margins(.01, .03)
    plt.show()

    plt.fill_between(time_labels[0:50], speed[0:50], 0, label='Wind Speed (mph)', color='#f77351')
    plt.fill_between(time_labels[0:50], gusts[0:50], speed[0:50], label='Wind Gust Speed (mph)', color='#5584b6')
    plt.legend()
    plt.xticks(np.arange(len(time_labels[0:50])), time_labels[0:50], rotation=90)
    plt.title('Wind Speed vs Time')
    plt.ylabel('Wind Speed (mph)').set_fontsize(12)
    plt.xlabel('Time (minutes)').set_fontsize(12)
    plt.margins(.01, .03)
    plt.show()

    plt.fill_between(time_labels[2000:2050], speed[2000:2050], 0, label='Wind Speed (mph)', color='#f77351')
    plt.fill_between(time_labels[2000:2050], gusts[2000:2050], speed[2000:2050],  label='Wind Gust Speed (mph)',
                     color='#5584b6')
    plt.legend()
    plt.xticks(np.arange(len(time_labels[2000:2050])), time_labels[2000:2050], rotation=90)
    plt.title('Wind Speed vs Time')
    plt.ylabel('Wind Speed (mph)').set_fontsize(12)
    plt.xlabel('Time (minutes)').set_fontsize(12)
    plt.margins(.01, .03)
    plt.show()


def plotscatter_difference(time, speed, gusts):
    """Create a line plot of the wind speeds and wind gusts over time"""
    # Get difference in speeds
    difference = gusts-speed
    plt.scatter(time, difference, label='Wind Gust Speed (mph)', color='green')
    plt.xticks(np.arange(len(time)), '', rotation=90, fontsize=8)
    plt.title('Speed Difference vs Time')
    plt.ylabel('Difference in Speed (mph)').set_fontsize(12)
    plt.xlabel('Time (minutes)').set_fontsize(12)
    plt.margins(.01, .03)
    plt.legend()
    plt.show()


def roundTime(dt=None, roundTo=60):
   """Round a datetime object to any time lapse in seconds
   dt : datetime.datetime object, default now.
   roundTo : Closest number of seconds to round to, default 1 minute.
   Author: Thierry Husson 2012 - Use it as you want but don't blame me.
   """
   if dt == None :
       dt = datetime.datetime.now()
   seconds = (dt.replace(tzinfo=None) - dt.min).seconds
   rounding = (seconds+roundTo/2) // roundTo * roundTo
   return dt + datetime.timedelta(0,rounding-seconds,-dt.microsecond)


def average_direction(speed, direction):
    """Calculate the average wind direction using wind speed and direction"""
    direction_ew = [speed[i]*np.sin(direction[i]*np.pi/180) for i in range(0, len(direction), 1)]
    direction_ew_average = [sum(direction_ew[i:i+30])/30 for i in range(0, len(direction_ew), 30)]
    direction_ns = [speed[i]*np.cos(direction[i]*np.pi/180) for i in range(0, len(direction), 1)]
    direction_ns_average = [sum(direction_ns[i:i+30])/30 for i in range(0, len(direction_ns), 30)]
    direction_average = np.arctan2(direction_ew_average,direction_ns_average) * 180/np.pi
    for i in range(0, len(direction_average), 1):
        if direction_average[i] < 0:
            direction_average[i] = direction_average[i] + 360
    return direction_average


def general_direction(wind_direction):
    """Get the general direction of the wind based on the """
    if wind_direction >= 0 and wind_direction < 23:
        return 'North'
    elif wind_direction >= 23 and wind_direction < 68:
        return 'Northeast'
    elif wind_direction >= 68 and wind_direction < 113:
        return 'East'
    elif wind_direction >= 113 and wind_direction < 158:
        return 'Southeast'
    elif wind_direction >= 158 and wind_direction < 203:
        return 'South'
    elif wind_direction >= 203 and wind_direction < 248:
        return 'Southwest'
    elif wind_direction >= 248 and wind_direction < 293:
        return 'West'
    elif wind_direction >= 293 and wind_direction < 338:
        return 'Northwest'
    elif wind_direction >= 338 and wind_direction < 360:
        return 'North'


def create_graphs(total_meas):
    # create list of vector colors for dimensions
    vector_color = ['#084594', '#ae1082', '#e41a1c', '#f37625', '#feb24c', '#a7b43d',
                    '#4daf4a', '#0082a3']
    gen_dir_types = ['North', 'Northeast', 'East', 'Southeast', 'South', 'Southwest', 'West', 'Northwest']
    """ call the various functions to plot the measurement data to answer the analysis questions."""
    # Create quiver plot for wind speed and direction
    plot_quiver(np.array(total_meas[3]), np.array(total_meas[5]), np.array(total_meas[6]), vector_color, gen_dir_types)

    # Create scatter plot for wind speed and time
    scatter_plot(np.array(total_meas[3]), np.array(total_meas[5]), np.array(total_meas[6]), vector_color, gen_dir_types)
    scatter_time(np.array(total_meas[0]), np.array(total_meas[3]), np.array(total_meas[5]), np.array(total_meas[6]), vector_color, gen_dir_types)

    # Create bar plot for wind speed and direction
    # This plot was not used after consideration of results it shows.
    #plotbar_speed(np.array(total_meas[0]), np.array(total_meas[3]), np.array(total_meas[5]), vector_color, gen_dir_types)

    # Create pie graph to show the breakdown of wind directions
    plotpie_direction(np.array(total_meas[6]), vector_color)

    # Create bar graph to plot the wind speed and direction
    # chose not to use this plot
    #plotbar_direction(np.array(total_meas[6]), vector_color)

    # call windrose plot to make windrose diagram of wind for direction plot
    plot_windrose(np.array(total_meas[3]), np.array(total_meas[5]))

    # Create Scatter plot for wind direction and time
    scatter_direction(np.array(total_meas[0]), np.array(total_meas[3]), np.array(total_meas[5]), np.array(total_meas[6]), vector_color, gen_dir_types)

    # Create bar graph showing wind speed and wind gusts
    plotbar_winds(np.array(total_meas[0]), np.array(total_meas[3]), np.array(total_meas[4]))

    # Create Fill Plot showing wind speeds and wind gusts
    filled_winds(np.array(total_meas[0]), np.array(total_meas[3]), np.array(total_meas[4]))

    # Create line graph showing wind speeds and wind gusts
    plotscatter_difference(np.array(total_meas[0]), np.array(total_meas[3]), np.array(total_meas[4]))

